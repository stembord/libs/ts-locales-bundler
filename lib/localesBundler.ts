import { IJSONObject, isJSONObject } from "@aicacia/json";
import { detailedDiff } from "deep-object-diff";
import path = require("path");
import through = require("through2");
import File = require("vinyl");

export interface IBundlerOptions {
  flatten?: boolean;
  minify?: boolean;
}

export const localesBundler = (options: IBundlerOptions) => {
  const locales: ILocale[] = [];

  options = options || {};

  options.flatten = options.flatten === true;
  options.minify = options.minify === true;

  return through.obj(
    (file, enc, callback) => {
      try {
        createLocales(file, locales, options);
        callback();
      } catch (e) {
        callback(e);
      }
    },
    function(callback) {
      const json = localesToJSONObject(locales, options);

      Object.keys(json).forEach(locale => {
        const locales = json[locale],
          fileName = locale + ".json";

        const contents = options.minify
          ? JSON.stringify(locales)
          : JSON.stringify(locales, null, 2);

        this.push(
          new File({
            path: fileName,
            contents: Buffer.from(contents)
          })
        );
      });

      callback();
    }
  );
};

export interface IDiffOptions {
  locale?: string;
}

export const localesDiff = (options: IDiffOptions) => {
  const locales: ILocale[] = [];

  const bundlerOptions = { flatten: true };

  options = options || {};
  options.locale = options.locale || "en";

  return through.obj(
    (file, enc, callback) => {
      try {
        createLocales(file, locales, bundlerOptions);
        callback();
      } catch (e) {
        callback(e);
      }
    },
    function(callback) {
      const json = localesToJSONObject(locales, bundlerOptions),
        jsonLocale = json[options.locale as string];

      delete json[options.locale as string];

      Object.keys(json).forEach(locale => {
        const detail: any = detailedDiff(
          jsonLocale as object,
          json[locale] as object
        );

        this.push({
          locale,
          added: Object.keys(detail.added),
          deleted: Object.keys(detail.deleted)
        });
      });

      callback();
    }
  );
};

const createLocales = (
  file: any,
  locales: ILocale[],
  options: IBundlerOptions
) => {
  const relativePath = path.relative(file.base, path.dirname(file.path)),
    paths = relativePath.split(path.sep),
    locale = paths.shift() as string,
    key = path.basename(file.path, path.extname(file.path));

  let messages = {
    [key]: JSON.parse(file.contents)
  };

  paths.forEach(key => {
    const out: IJSONObject = {};
    out[key] = messages;
    messages = out;
  });

  if (options.flatten) {
    messages = flattenJSONObject(messages);
  }

  locales.push({
    locale,
    messages
  });
};

interface ILocale {
  locale: string;
  messages: IJSONObject;
}

const flattenJSONObject = (json: IJSONObject) =>
  Object.keys(json).reduce(
    (acc, key) => {
      const value = json[key];

      if (isJSONObject(value)) {
        const flatObject = flattenJSONObject(value);

        Object.keys(flatObject).reduce((subAcc, subKey) => {
          subAcc[key + "." + subKey] = flatObject[subKey];
          return subAcc;
        }, acc);
      } else {
        acc[key] = json[key];
      }

      return acc;
    },
    {} as IJSONObject
  );

const localesToJSONObject = (
  locales: ILocale[],
  options: IBundlerOptions
): IJSONObject =>
  locales.reduce(
    (acc, chunk) => {
      const messages = (acc[chunk.locale] ||
        (acc[chunk.locale] = {})) as IJSONObject;

      extendMessages(messages, chunk.messages);

      return acc;
    },
    {} as IJSONObject
  );

const extendMessages = (out: IJSONObject, messages: IJSONObject) => {
  return Object.keys(messages).reduce((acc, key) => {
    const message = messages[key];

    out[key] = isJSONObject(message)
      ? extendMessages((out[key] as any) || {}, message)
      : message;

    return acc;
  }, out);
};
